export interface ILoginValues {
  email: string;
  password: string;
}

export const INITIAL_LOGIN_VALUES: ILoginValues = {
  email: "",
  password: ""
};
