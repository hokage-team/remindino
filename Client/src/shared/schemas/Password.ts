import { string } from "yup";
import { UPPERCASE_LETTER_REGEXP } from "../helpers/regexps/UppercaseLetter";
import { NUMBER_REGEXP } from "../helpers/regexps/Number";

export const PASSWORD_SCHEMA = string()
  .min(8, "Password must contain at least 8 symbols or letters")
  .max(255, "Password must be lower than 255 symbols or letters")
  .matches(
    UPPERCASE_LETTER_REGEXP,
    "Password must contain at least 1 uppercase letter"
  )
  .matches(NUMBER_REGEXP, "Password must contain at least 1 number")
  .required("Required");
