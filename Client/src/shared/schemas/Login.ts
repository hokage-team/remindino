import { EMAIL_SCHEMA } from "./Email";
import { object } from "yup";
import { PASSWORD_SCHEMA } from "./Password";

export const LOGIN_SCHEMA = object().shape({
  email: EMAIL_SCHEMA,
  password: PASSWORD_SCHEMA
});
