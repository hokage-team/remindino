import { USERNAME_SCHEMA } from "./Username";
import { EMAIL_SCHEMA } from "./Email";
import { object } from "yup";
import { PASSWORD_SCHEMA } from "./Password";

export const SIGNUP_SCHEMA = object().shape({
  username: USERNAME_SCHEMA,
  email: EMAIL_SCHEMA,
  password: PASSWORD_SCHEMA
});
