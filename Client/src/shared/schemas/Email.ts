import { string } from "yup";

export const EMAIL_SCHEMA = string()
  .email("Invalid email")
  .required("Required");
