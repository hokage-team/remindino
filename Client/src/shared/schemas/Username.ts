import {string} from "yup";

export const USERNAME_SCHEMA = string()
.min(2, "Too Short!")
.max(50, "Too Long!")
.required("Required")