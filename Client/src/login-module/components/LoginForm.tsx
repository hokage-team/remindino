import React, { Component } from "react";
import { Formik, Form, Field, ErrorMessage } from "formik";
import {
  INITIAL_LOGIN_VALUES,
  ILoginValues
} from "../../shared/types/LoginValues";
import { LOGIN_SCHEMA } from "../../shared/schemas/Login";
import LoginErrorMessage from "../styles/LoginErrrorMessage";
import LoginField from "../styles/LoginField";
export default class LoginForm extends Component {
  render() {
    return (
      <>
        <Formik
          validationSchema={LOGIN_SCHEMA}
          initialValues={INITIAL_LOGIN_VALUES}
          onSubmit={(values: ILoginValues) => {
            //  LoginService.Login(values);
          }}
        />
        {() => {
          <Form>
            <Field name="email" component={LoginField} />
            <ErrorMessage name="email" component={LoginErrorMessage} />
            <Field name="password" component={LoginField} />
            <ErrorMessage name="password" component={LoginErrorMessage} />
          </Form>;
        }}
      </>
    );
  }
}
